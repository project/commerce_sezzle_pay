<?php

namespace Drupal\commerce_sezzle_pay\PluginForm;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_sezzle_pay\Plugin\Commerce\PaymentGateway\SezzlePayCheckoutInterface;
use Drupal\Core\Form\FormStateInterface;


class SezzlePayCheckoutForm extends PaymentOffsiteForm
{
    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        $extra = array();
        $extra = [
            'return_url' => $form['#return_url'],
            'cancel_url' => $form['#cancel_url'],
        ];

        /** @var PaymentInterface $payment */
        $payment = $this->entity;
        /** @var SezzlePayCheckoutInterface $payment_gateway_plugin */
        $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

        $data['public_key'] = $payment_gateway_plugin->getConfiguration()['public_key'];
        $data['private_key'] = $payment_gateway_plugin->getConfiguration()['private_key'];

        $extra += ["merchant_completes" => $payment_gateway_plugin->getConfiguration()['merchant_completes']];
        $extra += ["shipping_info" => $payment_gateway_plugin->getConfiguration()['shipping_info']];

        $sezzle_auth_response = $payment_gateway_plugin->doAuthentication($data);

        // If we didn't get a TOKEN back from Sezzle, then the we need to exit checkout.
        if (empty($sezzle_auth_response['token'])) {
            throw new PaymentGatewayException(sprintf('[Sezzle pay error #%s]: %s', $sezzle_auth_response['status'], $sezzle_auth_response['message']));
        }

        // Create order on Sezzle and get redirect url
        $sezzle_order_url = $payment_gateway_plugin->mapAndCreateSezzleOrder($payment, $extra, $sezzle_auth_response['token']);
        $sezzleCheckoutUrl = '';
        if (!property_exists($sezzle_order_url, 'checkout_url')) {
            // dd($sezzle_order_url->order->checkout_url);
            $sezzleCheckoutUrl = $sezzle_order_url->order->checkout_url;
        } else {
            $sezzleCheckoutUrl = $sezzle_order_url->checkout_url;
        }


        // WITH TOKEN AND ORDER DATA SEND IT TO REDIRECT BELOW
        return $this->buildRedirectForm($form, $form_state, $sezzleCheckoutUrl, $data, PaymentOffsiteForm::REDIRECT_GET);
    }
}
