<?php

namespace Drupal\commerce_sezzle_pay\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Review;

/**
 * Provides the review pane.
 *
 * @CommerceCheckoutPane(
 *   id = "customReview",
 *   label = @Translation("Custom Review"),
 *   default_step = "review",
 * )
 */
class SkipReview extends Review {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::buildPaneForm($pane_form, $form_state, $complete_form);
    if (!is_null($payment_gateway = $this->order->get('payment_gateway')
      ->first())) {
      $current_gateway = $payment_gateway->entity;
      // If Sezzle is selected, skip this step
      if ($current_gateway->getPluginId() == "sezzle_pay_redirect_checkout") {
        $next_step = $this->checkoutFlow->getNextStepId($complete_form["#step_id"]);
        $this->checkoutFlow->redirectToStep($next_step);
      }
    }

    return $pane_form;
  }

}
