<?php

namespace Drupal\commerce_sezzle_pay\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Sezzle pay method type.
 *
 * @CommercePaymentMethodType(
 *   id = "sezzle_pay",
 *   label = @Translation("Sezzle pay"),
 *   create_label = @Translation("Sezzle pay"),
 * )
 */
class SezzlePay extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->pluginDefinition['label'];
  }
}
