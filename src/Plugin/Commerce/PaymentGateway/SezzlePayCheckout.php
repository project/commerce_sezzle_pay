<?php

namespace Drupal\commerce_sezzle_pay\Plugin\Commerce\PaymentGateway;

use Drupal;
use Drupal\address\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product_bundles\Entity\ProductBundleVariationInterface;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\profile\Entity\Profile;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Entity\PaymentMethod;

/**
 * Provides the SezzlePay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "sezzle_pay_redirect_checkout",
 *   label = @Translation("SezzlePay (Redirect to sezzlepay)"),
 *   display_label = @Translation("SezzlePay"),
 *    forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_sezzle_pay\PluginForm\SezzlePayCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa",
 *   },
 * )
 */
class SezzlePayCheckout extends OffsitePaymentGatewayBase implements SezzlePayCheckoutInterface
{
    /**
     * The HTTP client.
     *
     * @var Client
     */
    protected $httpClient;

    /**
     * The price rounder.
     *
     * @var RounderInterface
     */
    protected $rounder;

    /**
     * The time.
     *
     * @var TimeInterface
     */
    protected $time;

    /**
     * The Messenger service.
     *
     * @var MessengerInterface
     */
    protected $messenger;

    /**
     * Constructs a new PaymentGatewayBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     * @param PaymentTypeManager $payment_type_manager
     *   The payment type manager.
     * @param PaymentMethodTypeManager $payment_method_type_manager
     *   The payment method type manager.
     * @param TimeInterface $time
     *   The time.
     * @param ClientInterface $client
     *   The client.
     * @param RounderInterface $rounder
     *   The price rounder.
     */


    public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $client, RounderInterface $rounder, MessengerInterface $messenger)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

        $this->httpClient = $client;
        $this->rounder = $rounder;
        $this->messenger = $messenger;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->get('plugin.manager.commerce_payment_type'),
            $container->get('plugin.manager.commerce_payment_method_type'),
            $container->get('datetime.time'),
            $container->get('http_client'),
            $container->get('commerce_price.rounder'),
            $container->get('messenger')
        );
    }


    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
                'public_key' => '',
                'private_key' => '',
                'shipping_info' => FALSE,
                'merchant_completes' => FALSE,
                'api_logging' => FALSE,
                'skip_review' => FALSE,
            ] + parent::defaultConfiguration();
    }

    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['public_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Public key'),
            '#description' => $this->t('This is the public key from the SezzlePay checkout.'),
            '#default_value' => $this->configuration['public_key'],
            '#required' => TRUE,
        ];

        $form['private_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Private key'),
            '#description' => $this->t('This is the private key from the SezzlePay checkout.'),
            '#default_value' => $this->configuration['private_key'],
            '#required' => TRUE,
        ];

        $form['shipping_info'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Collect shipping info on Sezzle Pay'),
            '#description' => $this->t('Optional flag to indicate if you would like Sezzle Pay to collect shipping information for  customer, default is FALSE'),
            '#default_value' => $this->configuration['shipping_info'],
            '#required' => FALSE,
        ];

        $form['merchant_completes'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Authorize only on Sezzle pay, complete checkout on Drupal site'),
            '#description' => $this->t('Optional flag to determine whether checkout will capture funds or only authorize, default is FALSE and that will capture funds, with TRUE, you will have to add extra code to complete order and capture funds, more info about this can be found in readme'),
            '#default_value' => $this->configuration['merchant_completes'],
            '#required' => FALSE,
        ];

        $form['skip_review'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Option to skip review step(pane) and go straight to Sezzle site, applicable only when sezzle payment selected'),
            '#default_value' => $this->configuration['skip_review'],
            '#required' => FALSE,
        ];

        $form['api_logging'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Log webhook messages onNotify for debugging'),
            '#default_value' => $this->configuration['api_logging'],
            '#required' => FALSE,
        ];

        $form['decoupled'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enable decoupled (headless) flow'),
            '#default_value' => isset($this->configuration['decoupled']) ? $this->configuration['decoupled'] : '',
            '#required' => FALSE,
        ];

        $form['version'] = [
            '#type' => 'select2',
            '#title' => $this->t('Sezzle version'),
            '#description' => $this->t('Select the version of the API'),
            '#options' => [
                'v1' => 'Version 1',
                'v2' => 'Version 2'
            ],
            '#required' => TRUE,
            '#default_value' => isset($this->configuration['version']) ? $this->configuration['version'] : 'v1',
            '#select2' => [
                'width' => '40%'
            ],
        ];

        return $form;
    }

    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);

        // Format webhook url and send it as config on submit
        $webhook_url = Url::fromRoute('commerce_payment.notify', ['commerce_payment_gateway' => $form_state->getValue("id"),], ['absolute' => FALSE])->toString();
        $values = $form_state->getValue($form['#parents']);
        $this->configuration['public_key'] = $values['public_key'];
        $this->configuration['private_key'] = $values['private_key'];
        $this->configuration['webhook_url'] = $webhook_url;
        $this->configuration['shipping_info'] = $values['shipping_info'];
        $this->configuration['merchant_completes'] = $values['merchant_completes'];
        $this->configuration['api_logging'] = $values['api_logging'];
        $this->configuration['skip_review'] = $values['skip_review'];
        $this->configuration['decoupled'] = $values['decoupled'];
        $this->configuration['version'] = isset($values['decoupled']) && $values['decoupled'] == 1 ? 'v2' : $values['version']; //decoupled 1

        // Send webhook config to Sezzle and save it as form is saved
        $keys['public_key'] = $values['public_key'];
        $keys['private_key'] = $values['private_key'];
        $sezzle_auth_response = $this->doAuthentication($keys);
        if ($this->getVersion() == 'v1') {
            $this->setWebHookConfig($webhook_url, $sezzle_auth_response['token']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function doAuthentication($keys)
    {
        // Get SezzlePay Token
        //TODO maybe use try/catch here
        $request = $this->httpClient->POST($this->getApiAuthenticationUrl(), [
            'json' => $keys,
        ]);
        $response = json_decode($request->getBody(), TRUE);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function getApiAuthenticationUrl()
    {
        return $this->generateSezzleApiUrl('apiAuthenticationUrl');
    }

    /**
     * Generate Sezzle API url's
     *
     * @param string $urlOption
     * @param array $additionalParams
     * @return string
     */
    private function generateSezzleApiUrl($urlOption, $additionalParams = []): string
    {
        $version = $this->getVersion();
        $url = '';
        $urlAddon = '';
        if ($this->getMode() == 'test') {
            $urlAddon = 'sandbox.';
        }
        $baseGatewayUrl = 'https://' . $urlAddon . 'gateway.sezzle.com/';
        switch ($urlOption) {
            case 'configuration':
                if ($version == 'v1') {
                    $url = $baseGatewayUrl . 'v1/configuration';
                }
                break;
            case 'redirectUrl':
                if ($version == 'v1' && isset($additionalParams['orderId'])) {
                    $url = 'https://' . $urlAddon . 'checkout.sezzle.com/?id=' . $additionalParams['orderId'];
                }
                break;
            case 'orderDetails':
                $ordersString = 'order';
                if ($this->configuration['decoupled'] == false && $version == 'v1') {
                    $ordersString = 'orders';
                }
                $url = $baseGatewayUrl . $version . '/' .
                    $ordersString . '/' . $additionalParams['orderUuid'];
                break;

            case 'checkoutUrl':
                $url = $baseGatewayUrl . $version . '/checkouts';
                if ($this->configuration['decoupled'] == false && $version == 'v2') {
                    $url = $baseGatewayUrl . $version . '/session';
                }
                break;
            case 'apiAuthenticationUrl':
                $url = $baseGatewayUrl . $version . '/authentication';
                break;
            case 'completeCheckoutUrl':
                $url = $baseGatewayUrl . $version . '/checkouts/' . $additionalParams['orderId'] . '/complete';
                break;
            case 'refund':
                $order = \Drupal::entityTypeManager()->getStorage('commerce_order')->load($additionalParams->getOrderId());
                $order_uuid = $order->uuid();
                $url = $baseGatewayUrl . $version . '/orders/' . $order_uuid . '/refund';
                if ($version == 'v2'){
                    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
                    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
                    $payment = reset($payments);
                    $order_uuid = $payment->getRemoteId();
                    $url = $baseGatewayUrl . $version . '/order/' . $order_uuid . '/refund';
              }
        }
        return $url;
    }

    /**
     * Return the version set in the configuration
     *
     * @return string
     */
    private function getVersion(): string
    {
        return $this->configuration['version'];
    }

    /**
     * {@inheritdoc}
     */
    public function setWebHookConfig($webhook_url, $token)
    {
        $url = $this->generateSezzleApiUrl('configuration');

        $request = $this->httpClient->POST($url, [
            'headers' => [
                'Authorization' => $token,
            ],
            'json' => [
                'webhook_url' => Drupal::request()->getSchemeAndHttpHost() . $webhook_url,
            ],
        ]);
        // There is no response body
        $response = json_decode($request->getStatusCode());

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function getRedirectUrl($orderId)
    {
        return $this->generateSezzleApiUrl('redirectUrl', ['orderId' => $orderId]);
    }

    /**
     * {@inheritdoc}
     */
    public function completeCheckout($orderId, $token)
    {
        $url = $this->generateSezzleApiUrl('completeCheckoutUrl', ['orderId' => $orderId]);

        $request = $this->httpClient->post($url, [
            'headers' => [
                'Authorization' => $token,
            ],
        ]);
        $response = json_decode($request->getBody(), TRUE);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function mapAndCreateSezzleOrder(PaymentInterface $payment, array $extra, $token)
    {
        $order = $payment->getOrder();
        $amount = $this->rounder->round($payment->getAmount());

        $merchant_completes = FALSE;
        if ($extra['merchant_completes'] == TRUE) {
            $merchant_completes = TRUE;
        }

        $requires_shipping_info = FALSE;
        if ($extra['shipping_info'] == TRUE) {
            $requires_shipping_info = TRUE;
        }

        $orderDescription = Drupal::entityTypeManager()
                ->getStorage('commerce_store')->loadDefault()->getName() . ': ';
        $itemTitleArray = [];
        foreach ($order->getItems() as $item) {
            $itemTitleArray[] = $item->getTitle();
        }
        $orderDescription .= implode(', ', $itemTitleArray);

        $cancelUrl = $extra['cancel_url'];
        $completeUrl = $extra['return_url'];

        if (strpos($cancelUrl, Drupal::request()->getSchemeAndHttpHost()) == -1) {
            $cancelUrl = Drupal::request()->getSchemeAndHttpHost() . $extra['cancel_url'];
        }
        if (strpos($completeUrl, Drupal::request()->getSchemeAndHttpHost()) == -1) {
            $completeUrl = Drupal::request()->getSchemeAndHttpHost() . $extra['return_url'];
        }
        // Prepare basic data
        $body_data = [
            'amount_in_cents' => $this->toMinorUnits($amount),
            'currency_code' => $amount->getCurrencyCode(),
            'order_description' => $orderDescription,
            'order_reference_id' => $order->uuid->getString(),
            'checkout_cancel_url' => $cancelUrl,
            'checkout_complete_url' => $completeUrl,
        ];

        // Prepare customer details from user profile or billing data
        if ($profile = $order->getCustomer()) {

            // Check if this is anonymous user
            if ($profile->uid->getString() != 0) {
                $customer_details = [
                    'first_name' => isset($profile->field_first_name) ? $profile->field_first_name->getString() : '',
                    'last_name' => isset($profile->field_last_name) ? $profile->field_last_name->getString() : '',
                    'email' => isset($profile->mail) ? $profile->mail->getString() : '',
                    'phone' => isset($profile->field_phone) ? $profile->field_phone->getString() : '',
                ];
                // Filter out empty values.
                $body_data += ["customer_details" => array_filter($customer_details)];
            }
        }

        // Prepare shipping address, check if the order references shipments.
        if ($requires_shipping_info == FALSE && $order->hasField('shipments') && !$order->get('shipments')
                ->isEmpty()) {
            // Gather the shipping profiles and send shipping information
            $shipping_profiles = [];

            // Loop over the shipments to collect shipping profiles.
            foreach ($order->get('shipments')->referencedEntities() as $shipment) {
                if ($shipment->get('shipping_profile')->isEmpty()) {
                    continue;
                }
                $shipping_profile = $shipment->getShippingProfile();
                $shipping_profiles[$shipping_profile->id()] = $shipping_profile;
            }

            // If there are more shipping profiles we will use first one
            $shipping_profile = reset($shipping_profiles);
            /** @var AddressInterface $address */
            $address = $shipping_profile->address->first();
            $name = $address->getGivenName() . ' ' . $address->getFamilyName();
            $shipping_address = [
                'name' => substr($name, 0, 32),
                'street' => substr($address->getAddressLine1(), 0, 100),
                'street2' => substr($address->getAddressLine2(), 0, 100),
                'city' => substr($address->getLocality(), 0, 40),
                'state' => substr($address->getAdministrativeArea(), 0, 40),
                'country_code' => $address->getCountryCode(),
                'postal_code' => substr($address->getPostalCode(), 0, 20),
                'phone_number' => $shipping_profile->field_phone->getString(),
            ];
            // Filter out empty values.
            $body_data += ["shipping_address" => array_filter($shipping_address)];
        }

        // Prepare billing address
        if ($order->getBillingProfile()) {

            $address = $order->getBillingProfile()->address->first();
            $first_name = $address->getGivenName();
            $last_name = $address->getFamilyName();
            $name = $first_name . ' ' . $last_name;
            $order_email = $order->getEmail();
            $phone = isset($order->getBillingProfile()->field_phone) ? $order->getBillingProfile()->field_phone->getString() : '';

            $billing_address = [
                'name' => substr($name, 0, 32),
                'street' => substr($address->getAddressLine1(), 0, 100),
                'street2' => substr($address->getAddressLine2(), 0, 100),
                'city' => substr($address->getLocality(), 0, 40),
                'state' => substr($address->getAdministrativeArea(), 0, 40),
                'country_code' => $address->getCountryCode(),
                'postal_code' => substr($address->getPostalCode(), 0, 20),
                'phone_number' => $phone,
            ];
            // Filter out empty values.
            $body_data += ["billing_address" => array_filter($billing_address)];

            // If this was anonymous user, fill in billing data
            if ($profile->uid->getString() == 0) {
                $customer_details = [
                    'first_name' => isset($first_name) ? $first_name : '',
                    'last_name' => isset($last_name) ? $last_name : '',
                    'email' => isset($order_email) ? $order_email : '',
                    'phone' => isset($phone) ? $phone : '',
                ];
                // Filter out empty values.
                $body_data += ["customer_details" => array_filter($customer_details)];
            }
        }

        // Flag that makes Sezzle pay collect shipping information from the customer
        $body_data += ["requires_shipping_info" => $requires_shipping_info];

        $currency_code = $amount->getCurrencyCode();

        $all_items = [];
        $order_description = "";
        // Add order item data.
        foreach ($order->getItems() as $item) {
            $product = $item->getPurchasedEntity();
            $sku = '';

            //check if it is a product bundle entity
            if (
                class_exists('\Drupal\commerce_product_bundles\Entity\ProductBundleVariation') &&
                $product instanceof ProductBundleVariationInterface
            ) {
                $skus = [];
                //iterate over the product bundle variations to fetch te skus of the product
                foreach ($product->getProductVariations() as $bundleVariation) {
                    $productVariations = ProductVariation::loadMultiple($bundleVariation['variation_ids']);
                    if ($productVariations) {
                        foreach ($productVariations as $variation) {
                            $skus[] = $variation->getSku();
                        }
                    }
                }
                $sku = implode(',', $skus);
            } else {
                $sku = $product->getSku();
            }
            $all_items[] = [
                'name' => $item->getTitle(),
                'sku' => $sku,
                'quantity' => round($item->getQuantity(), 0),
                "price" => [
                    'amount_in_cents' => $this->processPriceAmount($this->toMinorUnits($item->getTotalPrice())),
                    'currency' => $item->getTotalPrice()->getCurrencyCode(),
                ],
            ];
            $order_description .= $item->getTitle() . ' ' . 'x ' . round($item->getQuantity(), 0) . ', ';
        }
        $body_data += ["items" => array_filter($all_items)];
        $body_data['order_description'] = $this->t('Order has following items: ') . substr($order_description, 0, -2);


        // Initialize Shipping,Tax and Discounte prices, they need to be sent separately to Sezzle pay.
        $shipping_amount_init = new Price('0', $currency_code);
        $tax_amount_init = new Price('0', $currency_code);

        // Collect the adjustments (Shipping, Taxes and Discounts)
        $adjustments = [];
        foreach ($order->collectAdjustments() as $adjustment) {
            // Skip included adjustments.
            if ($adjustment->isIncluded()) {
                continue;
            }
            // Tax & Shipping adjustments need to be handled separately.
            if ($adjustment->getType() == 'shipping') {
                $shipping_amount_init = $shipping_amount_init->add($adjustment->getAmount());
            } // Add taxes that are not included in the items total.
            elseif ($adjustment->getType() == 'tax') {
                $tax_amount_init = $tax_amount_init->add($adjustment->getAmount());
            } else {
                // Collect other adjustments.
                $type = $adjustment->getType();
                $source_id = $adjustment->getSourceId();
                if (empty($source_id)) {
                    // Adjustments without a source ID are always shown standalone.
                    $key = count($adjustments);
                } else {
                    // Adjustments with the same type and source ID are combined.
                    $key = $type . '_' . $source_id;
                }

                if (empty($adjustments[$key])) {
                    $adjustments[$key] = [
                        'type' => $type,
                        'label' => (string)$adjustment->getLabel(),
                        'total' => $adjustment->getAmount(),
                    ];
                } else {
                    $adjustments[$key]['total'] = $adjustments[$key]['total']->add($adjustment->getAmount());
                }
            }
        }

        if (!empty($adjustments)) {
            $discounts = [];
            // Setup adjustments where disocounts
            foreach ($adjustments as $adjustment) {
                if ($adjustment['type'] == 'promotion') {
                    $adjustment_amount = $this->rounder->round($adjustment['total']);
                    $discounts[] = [
                        'name' => $adjustment['label'],
                        'amount' => [
                            'amount_in_cents' => $this->processPriceAmount($this->toMinorUnits($adjustment_amount)),
                            'currency' => $adjustment_amount->getCurrencyCode(),
                        ],
                    ];
                }
            }
            $body_data += ["discounts" => array_filter($discounts)];
        }

        // Send the shipping amount separately.
        if (!$shipping_amount_init->isZero()) {
            $shipping_amount_init = $this->rounder->round($shipping_amount_init);

            $shipping_amount['amount_in_cents'] = $this->processPriceAmount($this->toMinorUnits($shipping_amount_init));
            $shipping_amount['currency'] = $shipping_amount_init->getCurrencyCode();

            $body_data += ["shipping_amount" => $shipping_amount];
        }

        // Send the tax amount.
        if (!$tax_amount_init->isZero()) {
            $tax_amount_init = $this->rounder->round($tax_amount_init);
            $tax_amount['amount_in_cents'] = $this->processPriceAmount($this->toMinorUnits($tax_amount_init));
            $tax_amount['currency'] = $tax_amount_init->getCurrencyCode();
            $body_data += ["tax_amount" => $tax_amount];
        }

        // Flag that makes customer return to Drupal site and finish order there
        $body_data += ["merchant_completes" => $merchant_completes];

        $body_data = $this->prepareBodyData($body_data);
        // Create order in Sezzle system, get the order URL to redirect user to it
        try {
            $request = $this->httpClient->post($this->getCheckoutUrl(), [
                'json' => $body_data,
                'headers' => [
                    'Authorization' => $token,
                ],
            ]);

            $response = json_decode($request->getBody());
            if ($this->pureVersion2()) {
                $order->setData('sezzle_order_uuid', $response->order->uuid);
                $order->save();
            }
        } catch (RequestException $e) {
            watchdog_exception('commerce_sezzle_pay', $e->getMessage());
            $this->messenger->addMessage($this->t('There was a problem with your payment'));
            return new RedirectResponse(Url::fromRoute(Url::fromRoute('commerce_cart.page')->toString()));
        }

        return $response;
    }

    /**
     * If the API used is version 2 convert to price in cents
     *
     * @param [type] $priceAmount
     * @return void
     */
    private function processPriceAmount($priceAmount)
    {
        if ($this->pureVersion2()) {
            $priceAmount = (int)$priceAmount;
        }
        return $priceAmount;
    }

    /**
     * Checks if the used API version ONLY version 2 withouth the decoupled flag
     *
     * @return boolean
     */
    private function pureVersion2(): bool
    {

        if ($this->configuration['decoupled'] == false && $this->getVersion() == 'v2') {
            return true;
        }
        return false;
    }

    /**
     * Convert the data that was prepared for API V1 to data for V2
     *
     * @param array $data
     * @return array $data
     */
    private function prepareBodyData(array $data): array
    {
        if ($this->pureVersion2()) {
            $data['complete_url'] = ['href' => $data['checkout_complete_url'], 'method' => 'GET'];
            unset($data['checkout_complete_url']);

            $data['cancel_url'] = ['href' => $data['checkout_cancel_url'], 'method' => 'GET'];
            unset($data['checkout_cancel_url']);
            $billingAddressName = $this->separateFirstLastName($data['billing_address']['name']);
            $customer = [
                'email' => $data['customer_details']['email'],
                'first_name' => $billingAddressName->firstName,
                'last_name' => $billingAddressName->lastName,
                'phone' => $data['billing_address']['phone_number'],
                'billing_address' => $data['billing_address']
            ];
            if (isset($data['shipping_address'])) {
                $customer['shipping_address'] = $data['shipping_address'];
            }
            $data['customer'] = $customer;

            $orderIntent = 'CAPTURE';
            if ($data['merchant_completes'] == true) {
                $orderIntent = 'AUTH';
            }
            $data['order'] = [
                'intent' => $orderIntent,
                'reference_id' => $data['order_reference_id'],
                'description' => $data['order_description'],
                'order_amount' => [
                    'amount_in_cents' => $this->processPriceAmount($data['amount_in_cents']),
                    'currency' => $data['currency_code']
                ]
            ];
            unset(
                $data['order_reference_id'],
                $data['order_description'],
                $data['amount_in_cents'],
                $data['currency_code'],
                $data['customer_details'],
                $data['billing_address']
            );
        }

        return $data;
    }

    /**
     * Explodes the imploded full name
     *
     * @param string $fullName
     * @return \stdClass $nameObject
     */
    private function separateFirstLastName(string $fullName)
    {
        $nameObject = new \stdClass();
        $explodedName = explode(' ', $fullName);
        $nameObject->firstName = $explodedName[0];
        unset($explodedName[0]);
        $nameObject->lastName = implode(' ', $explodedName);
        return $nameObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getCheckoutUrl()
    {
        return $this->generateSezzleApiUrl('checkoutUrl');
    }

    /**
     * {@inheritdoc}
     */
    public function onReturn(OrderInterface $order, Request $request)
    {
        $payment_gateway_plugin = null;
        if (!is_null($order->get('payment_gateway')->first())) {
            $payment_gateway_plugin = $order->get('payment_gateway')->first()->entity;
        }

        $configuration = [];
        /* Sezzle doesnt return anything when sending user back to drupal site, so we
                 * need to make an API call and get data from it. We only do this if
                 * merchant_completes is set to FALSE, if TRUE, then order needs to be set to
                 * Complete somewhere else, on some manual or auto approve action, we skip
                 * the rest if merchant_completes == 1
                 */

        if ($this->configuration['decoupled'] == true) {
            $decoupledData = $this->onReturnDecoupled($order, $payment_gateway_plugin, $request);
            $configuration = $decoupledData['configuration'];
            $payment_gateway_plugin = $decoupledData['payment_gateway_plugin'];
        }

        if (!count($configuration)) {
            $configuration = $payment_gateway_plugin->get("configuration");
        }

        if ($configuration['merchant_completes']) {
            return;
        }

        $data = [];
        $data['public_key'] = $configuration['public_key'];
        $data['private_key'] = $configuration['private_key'];

        $sezzle_auth_response = $this->doAuthentication($data);
        $token = $sezzle_auth_response['token'];
        $order_uuid = $order->uuid->getString();

        if ($this->configuration['decoupled'] == true && $request->query->has('session')) {
            $order_uuid = $request->get('session');
        }
        if ($this->pureVersion2()) {
            $order_uuid = $order->getData('sezzle_order_uuid');
        }


        // Get sezzle order data, it will be null if it is not captured
        $sezzle_order = $this->getOrderDetails($order_uuid, $token);

        $captured = false;
        if ($this->configuration['decoupled'] == true && $request->query->has('session')) {
            $captured = $sezzle_order['authorization']['approved'];
        } elseif ($this->pureVersion2()) {
            $captured = $sezzle_order["authorization"]['approved'];
        } else {
            $captured = !is_null($sezzle_order["captured_at"]);
        }
        try {
            if ($captured) {

                if ($this->configuration['decoupled'] == true && isset($sezzle_order['customer']['shipping_address'])) {
                    $sezzle_order['shipping_address'] = $sezzle_order['customer']['shipping_address'];
                    $sezzle_order['shipping_info'] = $sezzle_order['customer']['shipping_address'];
                }

                if ($this->getConfiguration()['shipping_info'] == TRUE && isset($sezzle_order['shipping_address'])) {
                    //prepare the Sezzle order response
                    $shippingAddress = $sezzle_order['shipping_address'];
                    $names = '';
                    $given_name = '';
                    $family_name = '';
                    if (is_array($shippingAddress) && isset($shippingAddress['name'])) {
                        $names = explode(' ', $shippingAddress['name']);
                        $given_name = array_shift($names);
                        $family_name = implode(' ', $names);
                    }

                    $shippingProfile = NULL;

                    //if the user is authenticated
                    if ($order->getCustomerId() != 0) {
                        //load all profiles to see if shipping address already exists to prevent database bloat
                        $profiles = Drupal::entityTypeManager()->getStorage('profile')
                            ->loadByProperties([
                                'uid' => $order->getCustomerId(),
                            ]);
                        foreach ($profiles as $profile) {
                            //check if the address exists
                            if (
                                $profile->address->country_code == $shippingAddress['country_code'] &&
                                $profile->address->locality == $shippingAddress['city'] &&
                                $profile->address->postal_code == $shippingAddress['postal_code'] &&
                                $profile->address->address_line1 == $shippingAddress['street'] &&
                                $profile->address->given_name == $given_name &&
                                $profile->address->family_name == $family_name
                            ) {
                                $shippingProfile = $profile;
                                break;
                            }
                        }
                    }

                    $orderShipments = $order->shipments->referencedEntities();
                    $firstShipment = reset($orderShipments);

                    //if profile exists set it as the order shipping profile else create new order
                    if (!is_null($shippingProfile)) {
                        $firstShipment->setShippingProfile($shippingProfile);
                    } else {
                        $profile = Profile::create([
                            'type' => 'customer',
                            'uid' => $order->getCustomerId(),
                        ]);
                        $profile->save();
                        if (!is_null($shippingAddress)) {
                            $profile->address->given_name = $given_name;
                            $profile->address->family_name = $family_name;
                            $profile->address->country_code = $shippingAddress['country_code'];
                            $profile->address->locality = $shippingAddress['city'];
                            $profile->address->administrative_area = $shippingAddress['state'];
                            $profile->address->postal_code = $shippingAddress['postal_code'];
                            $profile->address->address_line1 = $shippingAddress['street'];
                            $profile->address->address_line2 = $shippingAddress['street2'];

                            $profile->save();
                        }
                        $firstShipment->setShippingProfile($profile);
                    }
                    //save the new address
                    $firstShipment->save();
                }
                $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

                $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
                $payment = null;

                if ($payments) {
                    $payment = reset($payments);
                    $payment->setState('completed');
                    $payment->setAmount($order->getTotalPrice());
                } else {
                    $payment = $payment_storage->create([
                        'state' => 'completed',
                        'amount' => $order->getTotalPrice(),
                        'payment_gateway' => $this->parentEntity->id(),
                        'order_id' => $order->id(),
                    ]);
                }
                $payment->setRemoteId($order_uuid);
                $payment->save();

              $paymentMethod = PaymentMethod::create([
                'type' => 'sezzle_pay',
                'payment_gateway' => $this->parentEntity->id(),
                'payment_gateway_mode' => $this->getMode(),
                'order_id' => $order->id(),
                'uid' => $order->getCustomerId(),
                'remote_id' => $payment->getRemoteId(),
              ]);
              $paymentMethod->setReusable(FALSE);
              $paymentMethod->setBillingProfile($order->getBillingProfile());
              $paymentMethod->save();

              $payment->payment_method = $paymentMethod->id();
              $payment->save();

              $order->set('payment_method', $paymentMethod);
              $order->save();

            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Returnes data needed for Decoupled order
     *
     * @param OrderInterface $order
     * @param [type] $payment_gateway_plugin
     * @param Request $request
     * @return void
     */
    private function onReturnDecoupled(OrderInterface $order, $payment_gateway_plugin, Request $request)
    {
        if (is_null($payment_gateway_plugin)) {
            $paymentGateway = Drupal::entityTypeManager()
                ->getStorage('commerce_payment_gateway')
                ->load($request->get('payment_gateway'));

            $payment_gateway_plugin = $paymentGateway->getPlugin();
            $configuration = $payment_gateway_plugin->configuration;
            return compact('paymentGateway', 'payment_gateway_plugin', 'configuration');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderDetails($order_uuid, $token, $include_shipping = TRUE)
    {
        $url = $this->generateSezzleApiUrl('orderDetails', ['orderUuid' => $order_uuid]);

        $request = $this->httpClient->get($url, [
            'headers' => [
                'Authorization' => $token,
            ],
            'json' => [
                'include-shipping-info' => $include_shipping,
            ],
        ]);
        $response = json_decode($request->getBody(), TRUE);

        return $response;
    }

    public function onNotify(Request $request)
    {
        // Log the response message if request logging is enabled.
        if (!empty($this->configuration['api_logging'])) {
            Drupal::logger('commerce_sezzle_pay')
                ->debug('e-Commerce notification: <pre>@body</pre>', [
                    '@body' => var_export($request->query->all(), TRUE),
                ]);
        }
        // Get Webhook request data.
        $webhook_data = $this->getRequestDataArray($request->getContent());
        //TODO get this data somehow and then act on it, make refund or order update
        // https://docs.drupalcommerce.org/commerce2/developer-guide/payments/create-payment-gateway/on-site-gateways/creating-payments

    }

    /**
     * Get data array from a request content.
     *
     * @param string $request_content
     *   The Request content.
     *
     * @return array
     *   The request data array.
     */
    protected function getRequestDataArray($request_content)
    {
        parse_str(html_entity_decode($request_content), $webhook_data);
        return $webhook_data;
    }

    /**
     * {@inheritdoc}
     */
    public function onCancel(OrderInterface $order, Request $request)
    {
        $this->messenger->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
            '@gateway' => $this->getDisplayLabel(),
        ]));
    }
  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    $version = $this->getVersion();
    try {
    $url = $this->generateSezzleApiUrl('refund', $payment);
    $amount_number = ($this->toMinorUnits($amount));
    if ($version == 'v1'){
      $keys = [
        "amount" => [
          "amount_in_cents" => $amount_number,
          "currency" => $amount->getCurrencyCode(),
        ]
      ];
    } else{
      $keys = [
          "amount_in_cents" => $amount_number,
          "currency" => $amount->getCurrencyCode(),
      ];
    }
    $data = [];
    $data['public_key'] = $this->getConfiguration()["public_key"];
    $data['private_key'] = $this->getConfiguration()["private_key"];

    $sezzle_auth_response = $this->doAuthentication($data);
    $token = $sezzle_auth_response['token'];

    $this->httpClient->post($url, [
      'json' => $keys,
      'headers' => [
        'Authorization' => $token,
      ],
    ]);
    }
    catch (\Stripe\Error\Base $e) {
      ErrorHelper::handleException($e);
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

}
