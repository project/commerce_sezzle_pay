<?php

namespace Drupal\commerce_sezzle_pay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the Express Checkout payment gateway.
 */
interface SezzlePayCheckoutInterface extends OffsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface{

  /**
   * Gets the redirect URL.
   *
   * @param string $webhook_url
   *   Order ID data needed for this request.
   * @param string $token
   *   Token obtained over doAuthentication() method
   * @return string
   *   Returns status of request
   */
  public function setWebHookConfig($webhook_url, $token);

  /**
   * Gets the redirect URL.
   *
   * @param array $order_id
   *   Order ID data needed for this request.
   * @return string
   *   The redirect URL.
   */
  public function getRedirectUrl($order_id);

  /**
   * Gets the redirect URL.
   *
   * @param string $order_id
   *   Order ID data needed for this request.
   * @param string $token
   *   Token obtained over doAuthentication() method
   * @param string $include_shipping
   *   Whether to include shipping info into response
   * @return array
   *   The array URL.
   */
  public function getOrderDetails($order_id, $token, $include_shipping);

  /**
   * Gets the authentication URL.
   *
   * @return string
   *   The authentication URL.
   */
  public function getApiAuthenticationUrl();

  /**
   * Gets the checkout URL.
   *
   * @return string
   *   The Checkout URL.
   */
  public function getCheckoutUrl();

  /**
   * Gets the auth data.
   *
   * @param array $keys
   *   Public and privete keys needed for this request.
   * @return array
   *   Sezzle auth response data.
   */
  public function doAuthentication($keys);

  /**
   * Gets the redirect URL.
   *
   * @param string $order_id
   *   Order ID data needed for this request.
   * @param string $token
   *   Token obtained over doAuthentication() method
   * @return array
   *   Sezzle response data.
   */
  public function completeCheckout($order_id, $token);

  /**
   * Gets the sezzle order data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $extra
   *   Extra data needed for this request.
   * @param array $token
   *   Token data needed for this request.
   * @return array
   *   Sezzle order response data.
   */
  public function mapAndCreateSezzleOrder(PaymentInterface $payment, array $extra, $token);

}
