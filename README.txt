INTRODUCTION
-------------
This module integrates Commerce Sezzle pay with Drupal Commerce, providing a tokenized payment gateway.
Customers can make payments in your Drupal Commerce shop, paying for products with payment installments.
Sezzle pay is offsite payment gateway that redirects users to their site and brings them back once payment is done.

REQUIREMENTS
------------
There are no special requirements other then to use drupal commerce suite

INSTALLATION
-------------
-Install and activate Commerce Sezzle pay module
-Create an account at https://dashboard.sezzle.com/ to get API keys
-Create new payment gateway at admin/commerce/config/payment-gateways as Sezzle Pay
-Insert your API private and public keys from the Sezzle pay Dashboard into this new gateway and save

CONFIGURATION
-------------
Go to admin/commerce/config/payment-gateways and edit Sezzle Pay gateway you previously added

DECOUPLED(HEADLESS) setup
-------------
Go to admin/commerce/config/payment-gateways and edit Sezzle Pay gateway you previously added
Select the decoupled(headless) flag. Headless flag automatically sets the API version to V2
On your frontend use the Sezzle SDK.
When you complete the checkout on the return url add an additional parameter session with the order uuid as a value.
After that the plugin will return you to your current flow

merchant_completes FLAG
-----------------------
If you plan to manually capture funds, you can set this flat to TRUE in payment config.
In that case you will have to manually call Sezzle Pay endpoint to complete checkout and get the funds. To do that you need to call
POST https://gateway.sezzle.com/v1/checkouts/{order_reference_id}/complete
in some custom code you make, more info about that can be found here
https://docs.sezzle.com/#create-a-checkout
There is also a ready method for that in SezzlePayCheckout class, completeCheckout() that you can run

Profile fields and billing profile
----------------------
To sent customer details to sezzle pay, this module expects to have this fields in profile of user
$profile->field_first_name, $profile->field_last_name, $profile->mail,  $profile->field_phone
If not, then nothing will be sent to sezzle pay.

To collect phone number on billing profile, you need to add field_phone to billing
it is collected as $order->getBillingProfile()->field_phone